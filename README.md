# Social Network
### Настройка:
```
npm install
npm run start
```
### Скриншоты:
Главная страница:
![Image](./screenshots/main_page.png)
Профиль:
![Image](./screenshots/profile.png)
Запись:
![Image](./screenshots/post.png)
Настройка профиля:
![Image](./screenshots/setting.png)
Добавление новой записи:
![Image](./screenshots/add_photo.png)
Форма входа:
![Image](./screenshots/login.png)
Форма регистрации:
![Image](./screenshots/register.png)
Поиск пользовтеля:
![Image](./screenshots/search.png)
Ошибка 404:
![Image](./screenshots/404.png)
